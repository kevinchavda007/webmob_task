package com.aswdc.apiapplication.Design;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.aswdc.apiapplication.Api.ApiClient;
import com.aswdc.apiapplication.Api.ApiInterface;
import com.aswdc.apiapplication.Bean.BeanService;
import com.aswdc.apiapplication.Bean.Data;
import com.aswdc.apiapplication.Bean.PurchasedOfficeService;
import com.aswdc.apiapplication.Bean.PurchasedService;
import com.aswdc.apiapplication.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ListView lst;
    ApiInterface apiService;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Service List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lst=(ListView)findViewById(R.id.service_list);
        pd=new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        getdata();
    }

    void getdata(){
        pd.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<PurchasedOfficeService> call = apiService.getClass();
        call.enqueue(new Callback<PurchasedService>() {
            @Override
            public void onResponse(Call<PurchasedService>call, Response<PurchasedService> response) {
                if (response.body().getId() ==1){
                    List<PurchasedService> countryList = response.body().getId();
                    //lst.setAdapter(new Adapter_Country(CountryListActivity.this,countryList));
                }
                else{
                    Toast.makeText(MainActivity.this,"Data Not Found",Toast.LENGTH_LONG).show();
                }
                if (pd.isShowing())
                    pd.dismiss();
            }

            @Override
            public void onFailure(Call<PurchasedService>call, Throwable t) {
                if (pd.isShowing())
                    pd.dismiss();
                Toast.makeText(MainActivity.this,"Server Not Responding",Toast.LENGTH_LONG).show();
            }
        });
    }
}