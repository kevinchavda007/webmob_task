package com.aswdc.apiapplication.Api;

import com.aswdc.apiapplication.Bean.Data;
import com.aswdc.apiapplication.Bean.PurchasedOfficeService;
import com.aswdc.apiapplication.Bean.PurchasedService;

import retrofit2.Call;
import retrofit2.http.GET;

public class ApiInterface {
    @GET("b/5efdf1000bab551d2b6ab1c9/1")
    Call<PurchasedOfficeService> getServiceList();
}
