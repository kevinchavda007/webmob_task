package com.aswdc.apiapplication.Bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PurchasedOfficeTemplate {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("office_id")
    @Expose
    private Integer officeId;
    @SerializedName("service_type_id")
    @Expose
    private Integer serviceTypeId;
    @SerializedName("purchased_office_services")
    @Expose
    private List<PurchasedOfficeService> purchasedOfficeServices = null;
    @SerializedName("__typename")
    @Expose
    private String typename;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOfficeId() {
        return officeId;
    }

    public void setOfficeId(Integer officeId) {
        this.officeId = officeId;
    }

    public Integer getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(Integer serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public List<PurchasedOfficeService> getPurchasedOfficeServices() {
        return purchasedOfficeServices;
    }

    public void setPurchasedOfficeServices(List<PurchasedOfficeService> purchasedOfficeServices) {
        this.purchasedOfficeServices = purchasedOfficeServices;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

}
