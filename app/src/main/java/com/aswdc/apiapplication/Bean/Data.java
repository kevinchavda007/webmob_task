package com.aswdc.apiapplication.Bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
    @SerializedName("purchased_services")
    @Expose
    private List<PurchasedService> purchasedServices = null;

    public List<PurchasedService> getPurchasedServices() {
        return purchasedServices;
    }

    public void setPurchasedServices(List<PurchasedService> purchasedServices) {
        this.purchasedServices = purchasedServices;
    }
}
