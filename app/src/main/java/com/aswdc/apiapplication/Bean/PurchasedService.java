package com.aswdc.apiapplication.Bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PurchasedService {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("purchased_office_template")
    @Expose
    private PurchasedOfficeTemplate purchasedOfficeTemplate;
    @SerializedName("__typename")
    @Expose
    private String typename;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PurchasedOfficeTemplate getPurchasedOfficeTemplate() {
        return purchasedOfficeTemplate;
    }

    public void setPurchasedOfficeTemplate(PurchasedOfficeTemplate purchasedOfficeTemplate) {
        this.purchasedOfficeTemplate = purchasedOfficeTemplate;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }
}
