package com.aswdc.apiapplication.Bean;

import android.provider.ContactsContract;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BeanService {
    @SerializedName("data")
    @Expose
    private ContactsContract.Contacts.Data data;

    public ContactsContract.Contacts.Data getData() {
        return data;
    }

    public void setData(ContactsContract.Contacts.Data data) {
        this.data = data;
    }
}
